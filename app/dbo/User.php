<?php

namespace app\dbo;


/**
 * Description of User
 *
 * @author Christopher
 */
class User extends \rueckgrat\db\Mapper{
    
    protected $prename;
    protected $name;
    protected $mail;
    
    public function __construct(){
        parent::__construct();
    }
    
    public function getPrename(){
        return $this->prename;    
    }
    
    public function getName(){
        return $this->name;   
    }
    
    public function getMail(){
        return $this->mail;   
    }
    
    // I was trying to figure out how getID still display in the MainView
    // even though this function is being commented out. 
    /*public function getID(){
        //return $this->id;   
    }*/
    
}
