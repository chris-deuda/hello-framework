<?php

namespace app\controller;

use rueckgrat\mvc\DefaultController;
use \rueckgrat\security\Input;

/**
 * Description of Main
 *
 * @author Christopher
 */
class Main extends DefaultController{
    protected $userModel;
    protected $mainView;

    public function __construct(){
        parent::__construct();
        $this->userModel = new \app\model\UserModel();
        $this->mainView = new \app\view\MainView();
    }
    //http://localhost/hello-framework/?c=Main&m=helloFramework
    //http://localhost/hello-framework/?c=Main&m=index
    public function index(){
        $users = $this->userModel->getAllUsers();
        return $this->mainView->renderFrontPage( $users); //passing the value just like in codeigniter

    }
    
    public function create(){
        // initialize user class
        $user = new \app\mapper\User();
        //getting all result from POST method
        $user->map( array(
            'prename' => Input::p('prename'),
            'name' => Input::p('name'),
            'mail' => Input::p('mail')
        ));
        
        $this->userModel->createUser($user);
        
        header("Location: ?c=Main&m=index");
        
          
    }
    
    public function delete( ){
        
        $user = new \app\mapper\User();
        
        $user = $this->userModel->getById(Input::g('id'));
        
        $this->userModel->deleteUser($user);
        
        header("Location: ?c=Main&m=index");
        
    }
    
    public function edit(){
        $user = new \app\mapper\User();
        
        $user = $this->userModel->getById(Input::g('id'));
        
        return $this->mainView->renderEditPage( $user); //from index method
    }
    
    public function editUser(){
        $user = new \app\mapper\User();
          $user->map( array(
             'id' => Input::p('id'),
            'prename' => Input::p('prename'),
            'name' => Input::p('name'),
            'mail' => Input::p('mail')
        ));
        $this->userModel->editUser( $user );
        
        header("Location: ?c=Main&m=index");
    }

}
